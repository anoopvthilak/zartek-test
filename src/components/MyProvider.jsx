import React, { Component } from "react";
import MyContext from './MyContext.jsx';

export default class MyProvider extends Component {
  constructor(props){
    super(props);
    var cart_items = window.localStorage.getItem('resto_cafe_cart_items');
    cart_items = cart_items?JSON.parse(cart_items):[];
    this.state = {
      restaurant_name : "",
      cart_items : cart_items,
    }
  }

  render() {
    return (
      <MyContext.Provider
        value={{
          restaurant_name : this.state.restaurant_name,
          updateRestaurantName : (name) => {
            this.setState({
              restaurant_name : name
            });
          },
          cart_items : this.state.cart_items,
          updateCart : (cart_items) => {
              window.localStorage.setItem("resto_cafe_cart_items",JSON.stringify(cart_items));
              this.setState({
                  cart_items : cart_items,
              });
          },
        }}
      >
        {this.props.children}
      </MyContext.Provider>
    );
  }
}