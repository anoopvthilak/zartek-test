import {Component} from "react";
import $ from "jquery";
import "./Products.scss";
import ui from '../../assets/js/ui.js';
import MyContext from "../MyContext.jsx";

class Products extends Component{
  constructor(props){
    super(props);
    this.state = {
      table_menu : {
        table_menu_list : [
          {
            category_dishes : []
          }
        ]
      }
    }
    this.cart_items = JSON.parse(window.localStorage.getItem('resto_cafe_cart_items'))??[];

  }

  componentDidMount(){
    var obj_this = this;
    const url = "https://run.mocky.io/v3/a67edc87-49c7-4822-9cb4-e2ef94cb3099";
    $.ajax({
      url : url,
      type : "GET"
    }).done(function(return_data){
      console.log(return_data[0]);
      var table_menu = return_data[0];
      table_menu.table_menu_list.forEach( (category) => {
        category.category_dishes.forEach( (item) => {
          item.qty = 0;
          obj_this.cart_items.forEach((cart_item) => {
            if(item.dish_id == cart_item.dish_id){
              item.qty = cart_item.qty;
              return false;
            }
          })
        });
      });
      obj_this.context.updateRestaurantName(table_menu.restaurant_name);
      obj_this.setState({
        table_menu : table_menu
      },() => {
        ui.swipeMenu(".table_menu_container");
      });
    });
  }

  addQty(index,idx){
    var table_menu = this.state.table_menu;
    var item = table_menu.table_menu_list[index]['category_dishes'][idx];
    item.qty = ++item.qty;
    this.updateCartItems(item);
    this.setState({table_menu : table_menu});
  }

  minusQty(index,idx){
    var table_menu = this.state.table_menu;
    var item = table_menu.table_menu_list[index]['category_dishes'][idx];
    item.qty = item.qty-1 >0?item.qty-1:0;
    this.updateCartItems(item);
    this.setState({table_menu : table_menu});
  }

  qtyInputOnChange(index,idx,e){
    var table_menu = this.state.table_menu;
    var item = table_menu.table_menu_list[index]['category_dishes'][idx];
    item.qty = e.target.value;
    this.updateCartItems(item);
    this.setState({table_menu : table_menu});
  }

  qtyInputOnBlur(index,idx,e){
    var table_menu = this.state.table_menu;
    var item = table_menu.table_menu_list[index]['category_dishes'][idx];
    item.qty = e.target.value>1?e.target.value:0;
    this.updateCartItems(item);
    this.setState({table_menu : table_menu});
  }

  updateCartItems(item){
    var found = this.cart_items.filter((product) => (product.dish_id == item.dish_id));
    if(found.length){
      if(item.qty >0){
        const index = this.cart_items.findIndex(product => product.dish_id == item.dish_id);
        // console.log(index)
        this.cart_items[index]['qty'] = item.qty;
      }else{
        this.cart_items = this.cart_items.filter((product) => (product.dish_id != item.dish_id));
      }
    }else{
      if(item.qty >0)
        this.cart_items.push(item);
    }
    this.context.updateCart(this.cart_items);
  }

  render(){
    return(
      <div className="products_page_container">
        <div className="header_spacer"></div>
        <div className="table_menu_container uijs swipemenu">
          <div className="toggles_container" id="toggles_cont" 
            style={{gridTemplateColumns: "repeat("+this.state.table_menu.table_menu_list.length+",max-content)"}}
          >
           {this.state.table_menu.table_menu_list && 
            this.state.table_menu.table_menu_list.map((item,index) => (
              <div className="toggles" menu={index} key={index}>
                {item.menu_category}
              </div>
            ))}
          </div>
          <div className="list_blocks_container">
           {this.state.table_menu.table_menu_list && 
            this.state.table_menu.table_menu_list.map((item,index) => (
            <div className="lists_container" key={index} menu={index}>
              {item.category_dishes && item.category_dishes.map((product,idx) => (
              <div className="single_product_container" key={idx}
                style={{opacity:!product.dish_Availability?"0.6":""}}
              >
                <div className="product_type" >
                  <div className={"product_type_icon "+(product.dish_Type==2?"":"non_veg")} >
                    <div className="circle" ></div>
                  </div>
                </div>
                <div className="product_details_container">
                  <div className="product_name">{product.dish_name}</div>
                  <div className="product_price_calories_container">
                    <div className="product_price">{product.dish_currency} &nbsp;{product.dish_price}</div>
                    <div className="product_calories">{product.dish_calories} Calories</div>
                  </div>
                  <div className="product_description">{product.dish_description}</div>
                  <div className="qty_container" 
                    style={{display:product.dish_Availability?"inline-block":"none"}}
                  >
                    <button onClick={() => {this.minusQty(index,idx)}}><i className="fa fa-minus"></i></button>
                    <input className="product_qty" 
                      type="number" 
                      value={product.qty} 
                      min="0"
                      onChange={e => {this.qtyInputOnChange(index,idx,e)}} 
                      onBlur={e => {this.qtyInputOnBlur(index,idx,e)}} 
                    />
                    <button onClick={() => {this.addQty(index,idx)}}><i className="fa fa-plus"></i></button>
                  </div>
                  <div className="addon_details">{product.addonCat.length?"Customizations available":""}</div>
                  <div className="addon_details">{!product.dish_Availability?"Not available":""}</div>
                </div>
                <div className="product_image" style={{backgroundImage : "url('"+product.dish_image+"')"}}></div>
                </div>
              ))}
            </div>
           ))}
          </div>
        </div>
      </div>
    )
  }

}

export default Products;
Products.contextType = MyContext;