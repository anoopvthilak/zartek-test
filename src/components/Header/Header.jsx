import {Component} from "react";
import "./Header.scss";
import MyContext from "../MyContext.jsx";

class Header extends Component{
  render(){
    return(
      <div className="header_container">
        <div className="restaurant_name">{this.context.restaurant_name}</div>
        <div className="header_spacer"></div>
        <div className="my_orders">My orders</div>
        <div className="cart">
          <i className="fas fa-shopping-cart"></i>
          <div className="cart_count">{this.context.cart_items.length}</div>
        </div>
      </div>
    )
  }
}

export default Header;
Header.contextType = MyContext;