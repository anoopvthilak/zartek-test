import './App.css';
import {
  BrowserRouter,
  Route
} from "react-router-dom";

import MyProvider from "./components/MyProvider.jsx";
import Header from "./components/Header/Header.jsx";
import Products from "./components/Products/Products.jsx";

function App() {
  return (
    <MyProvider> 
        <BrowserRouter>
          <Route exact path="/" component={Products} />
          <Header />
        </BrowserRouter>
      </MyProvider>
  );
}

export default App;
